#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>      
#include <semaphore.h>
#include <unistd.h>     
#include <sys/types.h>  
#include <errno.h>      


typedef struct tcola
{
    char nombre[6];
    int dir;
    struct tcola *sig;
}tcola;

typedef struct nodo
{
    char nombre[6];
    int dir;
}nodo;





tcola *Cabeza_izq ;
tcola *Talon_izq ;

tcola *Cabeza_der ;
tcola *Talon_der ;

// cola que contiene todos los nombres de los autos que ya pasaron por el puente
tcola *Cabeza_buffer ;
tcola *Talon_buffer ;

//Cola que es protegida por el mutex para guardar datos a la hora de entrar datos durante la simulacion
static tcola *Cabeza_entrada;  
static tcola *Talon_entrada ;

static pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;


void agregar(tcola **Cabeza,tcola **Talon,char *nombre, int dir, int flag);
nodo desencolar(tcola **Cabeza,tcola**Talon);
int estavacia(tcola *Cabeza);

void imprimir(tcola **Cabeza,tcola**Talon){
	nodo Nodo;

    while(!estavacia(*Cabeza)){
        Nodo = desencolar(Cabeza,Talon);
        printf("%s\n", Nodo.nombre );
    }   	
}

int recorrer(tcola **Cabeza,tcola **Talon,char *nombre)
{
    int cond = 0;
    tcola *temp = malloc(sizeof(tcola));
    if(estavacia(*Cabeza)){
    	return 0;
    }
    else{
    	temp = *Cabeza;
	    while(temp != NULL && cond == 0){
	        if(strcmp(temp->nombre,nombre) != 0){
	        	temp = temp->sig;
	        }
	        else 
	        	return 1;
	    }
	}
    return 0;
}

void agregar(tcola **Cabeza,tcola **Talon,char *nombre, int dir, int flag) // funcion para agregar a una cola con manejo de mutex en el caso que ya comenzo la simulacion
{
    tcola *nuevo;
    nuevo=malloc(sizeof(tcola));
    if(nuevo!=NULL){
        strcpy(nuevo->nombre,nombre);
        nuevo->dir = dir;
        nuevo->sig = NULL;
        if (flag == 0){
	        if(estavacia(*Cabeza)){
	            *Cabeza=nuevo;
	        }else
	            (*Talon)->sig=nuevo;
	        *Talon=nuevo;
    	}else{
    		int error;
    		if(error = pthread_mutex_lock(&lock))
    			fprintf(stderr, "Failed to lock: %s\n", strerror(error));
    		if(estavacia(*Cabeza)){
	            *Cabeza=nuevo;
	        }else
	            (*Talon)->sig=nuevo;
	        *Talon=nuevo;
	        if(error = pthread_mutex_unlock(&lock))
    			fprintf(stderr, "Failed to unlock: %s\n", strerror(error));
    	}
    }
    else
        printf("\nNo es posible agregar a la cola!");
}



nodo desencolar(tcola **Cabeza,tcola**Talon)
{
    tcola *temp;
    nodo aux;

    temp=*Cabeza;
    strcpy(aux.nombre,(*Cabeza)->nombre);
    aux.dir = (*Cabeza)->dir;
    *Cabeza=(*Cabeza)->sig;
    if(*Cabeza==NULL){
        *Talon=NULL;
    }
    free(temp);
    return aux;
}

int estavacia(tcola *Cabeza)
{
    return (Cabeza == NULL);
}



/*Funcion que hace manejo de strings y manda a agregar a colas*/
int com_int(char *commando,int longitud,tcola **Cabeza, tcola **Talon, int flag){
	char *izq = "izq";
	char *der = "der";
	char *car = "car";
	char *status = "status";
	char *start = "start";
	char *fin = "fin";
	int dir;
	int rep;

	if(strcmp(commando,start)==0)
		printf("La simulacion se encuentra en ejecucion\n");
	else if(strcmp(commando,status)==0){
		//status Aqui falta la llamada a status
	}else if(strcmp(fin,commando)==0){
		return 2;
	}
	else{
		char *primerc = malloc(3);
		char *ultimc = malloc(3);
		char *nombre = malloc(6);

		strncpy(primerc,commando,3);
		strncpy(ultimc,commando + (longitud-3),3);

		if(strcmp(car,primerc)==0 && (strcmp(izq,ultimc)==0 || strcmp(der,ultimc)==0) && longitud == 14){
			
			if(commando[3] != ' ' || commando[10]!= ' '){
				printf("%s\n","Comando equivocado o nombre muy largo"  );
				return 0;
			}else{
				strncpy(nombre,commando + 4,6);

				if(strcmp(ultimc,izq) == 0){
					dir = 0;
				}else dir = 1;

				rep = recorrer(Cabeza,Talon,nombre);
				if(rep == 0){
					agregar(Cabeza, Talon, nombre, dir,0);

					if(flag == 0){
						if(dir == 0)
							agregar(&Cabeza_izq,&Talon_izq,nombre, dir,0);
						else if(dir == 1)
							agregar(&Cabeza_der,&Talon_der, nombre, dir,0);
					}else{
						agregar(&Cabeza_entrada,&Talon_entrada,nombre, dir,1);
					}
				}
				else
					printf("%s\n", "Nombre duplicado" );
			}

		}else{
			printf("%s\n","Comando equivocado o nombre muy largo"  );
			return 0;
		}
	}
	return 0;
}

/*Funcion que maneja la entrada de datos antes de haber ingresado "start"*/
int condition(tcola **Cabeza, tcola **Talon){
	char *start = "start";
	char *fin = "fin";
	
	int cond = 0;
	int longitud;

	while(cond == 0){
		printf("==>> ");
		char commandobuffer[30];
		fgets(commandobuffer,sizeof(commandobuffer),stdin);
		char *com = malloc(14);

		longitud = strlen(commandobuffer);
		if(longitud>15 || longitud<4){
			printf("%s\n","Comando equivocado o nombre muy largo" );
			cond = 0;
		}else{

			strncpy(com,commandobuffer,longitud-1);

			
			if(strcmp(start,com)==0){
				cond = 1;
			}else if(strcmp(fin,com)==0){
				cond = 2;
			}else{
				cond = com_int(com,longitud-1,Cabeza,Talon,0);
			}	
		
		}			
	}
	return cond;
}

/* Funcion del hilo de entrada de datos que maneja la entrada de datos durante la simulacion*/
void entrada ()
{
	int cond = 0;
	int longitud;
	while(cond == 0){
		printf("==>> ");
		char commandobuffer[30];
		fgets(commandobuffer,sizeof(commandobuffer),stdin);
		char *com = malloc(14);

		longitud = strlen(commandobuffer);
		if(longitud>15 || longitud<4){
			printf("%s\n","Comando equivocado o nombre muy largo" );
			cond = 0;
		}else{

			strncpy(com,commandobuffer,longitud-1);

			cond = com_int(com,longitud-1,&Cabeza_buffer, &Talon_buffer,1);
			
		}			
	}
	if( cond == 2)
		pthread_exit(0);
}