#include <stdio.h>
#include<string.h>
#include<unistd.h>
    void dibujar(int x, int y, int z, int a, int v[]);
int main(){
    int v1[14]={2,4,6,8,10,12,14,45,67,78,34,3,75,98};
    int v2[11]={1,3,5,7,9,11,13,40,76,41,29};
    int i,j,b=1,cant1,cant2,p,s,t,x,y,z;
    int ciclos; // variable que almacena cuantas veces se va a llamar a la funcion dibujar
    int der=0; // cantidad de autos que pasaron de izquierda a derecha
    int izq=0; // cantidad de autos que pasaron de derecha a izquierda
    int c1= sizeof(v1)/sizeof(int);
    int c2= sizeof(v2)/sizeof(int);
   while (der<c1 || izq<c2 ){
        if(b==1){ // para los que quieren ir a la derecha
            cant1= c1-der; // se calcula cuantos quieren pasar el puente a partir de los que ya han pasado
            if(cant1>5){ // si son mas de 5 entonces solo los primeros 5 pasan
                cant1=5;
            }
            if(cant1>0){
                ciclos=3+cant1;
            }else{
                ciclos=0;
            } // se calcula cuantas impresiones se hacen del puente
            p=(cant1*-1); // posicion para la primera parte del puente
            s=p-1; //posicion para la segunda parte del puente
            t=s-1; //posicion para la tercera parte del puente
            for(j=cant1; j<=(cant1 +ciclos -1); j++){
                x=j+p;
                y=j+s;
                z=j+t;
                if(x>=cant1){
                    x=-1;
                }
                if(y>=cant1){
                    y=-1;
                }
                if(z>=cant1){
                    z=-1;
                }
                system("clear");
                dibujar(x,y,z,der,v1);
                sleep(1);

            }
            der=der+cant1;

            b=0;

        }else{
            cant2= c2-izq;
             if(cant2>5){ // si son mas de 5 entonces solo los primeros 5 pasan
                cant2=5;
            }
            if(cant2>0){
                ciclos=3+cant2;
            }else{
                ciclos=0;
            }// se calcula cuantas impresiones se hacen del puente
            t=(cant2*-1);
            s=t-1;
            p=s-1;

            for(j=cant2; j<=(cant2 +ciclos -1); j++){
                x=j+p;
                y=j+s;
                z=j+t;
                if(x>=cant2){
                    x=-1;
                }
                if(y>=cant2){
                    y=-1;
                }
                if(z>=cant2){
                    z=-1;
                }
               system("clear");
                dibujar(x,y,z,izq,v2);
               sleep(1);

            }
            izq=izq+cant2;

            b=1;

        }
    }

    return 0;
}
 void dibujar(int x, int y, int z, int a, int v[]){ // Funcion que dibuja los 7 casos posibles del puente
     fflush(stdout);
    if(x<0 && y<0 && z<0){
        printf("-  -   - \n");
    }
    if(x>=0 && y<0 && z<0){
        printf("%d  -   - \n",v[x+a]);
    }
    if(x<0 && y>=0 && z<0){
        printf("-  %d   - \n",v[y+a]);
    }
    if(x<0 && y<0 && z>=0){
        printf("-  -   %d \n",v[z+a]);
    }
    if(x>=0 && y>=0 && z<0){
        printf("%d  %d   - \n",v[x+a],v[y+a]);
    }
    if(x<0 && y>=0 && z>=0){
        printf("-  %d   %d \n",v[y+a],v[z+a]);
    }
    if(x>=0 && y>=0 && z>=0){
        printf("%d  %d   %d \n",v[x+a],v[y+a],v[z+a]);
    }
 }

